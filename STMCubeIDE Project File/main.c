/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "string.h"
#include "sht2x_for_stm32_hal.h"
#include <stdio.h>



/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 ADC_HandleTypeDef hadc;

I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void debugPrintln(UART_HandleTypeDef *uart_handle,char _out[]){
	HAL_UART_Transmit(uart_handle, (uint8_t *) _out, strlen(_out), 60);
	char newline[2] = "\r\n";
	HAL_UART_Transmit(uart_handle, (uint8_t *)newline, 2, 10);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	  char msg[10];

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  MX_ADC_Init();
  /* USER CODE BEGIN 2 */

  /* Initializes SHT2x temperature/humidity sensor and sets the resolution. */
  	SHT2x_Init(&hi2c1);
  	SHT2x_SetResolution(RES_14_12);
  	char str[60] = { 0 }; //Useful buffer for printing to UART
  	uint8_t I2CReturn = 0; //Status var to indicate if HAL_I2C operation has succeeded (1) or failed (0);

  	//Setup variables for reading and writing
  	uint16_t EEPROM_DEVICE_ADDR = 0x54 << 1; //Address of EEPROM device on I2C bus

  	uint16_t madd = 0x00; //Memory address variable containing a memory address to a location in the eeprom.
  	uint8_t *smadd= &madd;
  	uint8_t maddResult = 0x00;
  	uint8_t *rmadd = &maddResult;
  	uint8_t analogData = 0x69;//Data variable containing sStarting value to write to memory, could be any 8bit value
  	uint8_t *sanalogData = &analogData; //Pointer to sending Data variable
  	uint8_t Result = 0x00; //Variable to stored value read back from memory in
  	uint8_t *ranalogData = &Result; //Pointer to result data variable

  	uint8_t digitalData = 0x666;//Data variable containing sStarting value to write to memory, could be any 8bit value
  	uint8_t *sdigitalData = &digitalData; //Pointer to sending Data variable
  	uint8_t resultDigital = 0x00; //Variable to stored value read back from memory in
  	uint8_t *rdigitalData = &resultDigital; //Pointer to result data variable
  	//Say hello over UART
//  	debugPrintln(&huart1, "Hello, this is STMF0 Discovery board: ");
  	uint16_t testLight=0;



  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  unsigned char buffer[100] = { 0 };
	  	  	  	float cel = SHT2x_GetTemperature(1);
	  	  	  	digitalData = cel ;
	  	  		/* Converts temperature to degrees Fahrenheit and Kelvin */
	  	  		float fah = SHT2x_CelsiusToFahrenheit(cel);
	  	  		float kel = SHT2x_CelsiusToKelvin(cel);
	  	  		float rh = SHT2x_GetRelativeHumidity(1);

	  	  		HAL_Delay(10000);


	      // Get ADC value
	      HAL_ADC_Start(&hadc);
	      HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);
	      testLight = HAL_ADC_GetValue(&hadc);
	      analogData = HAL_ADC_GetValue(&hadc);
	      debugPrintln(&huart1, "Light Test: "); // print full line
	      sprintf(msg, "%hu\r\n", testLight);
	      HAL_UART_Transmit(&huart1, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);


	  		HAL_Delay(1000);

		  	  //WRITING the Max Address to position 0x00 in EEPROM
		  	  memset(str, 0, sizeof(str));
		  	  sprintf(str, "Updating 0x%X to EEPROM address 0x%X", madd, 0x00);
		  	  debugPrintln(&huart1, str);
		  	  I2CReturn = HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEVICE_ADDR, madd, 2, sanalogData, 1, HAL_MAX_DELAY);
		  	  if (I2CReturn != HAL_OK) {
		  	  debugPrintln(&huart1, "Write to address FAILED");
		  	  }

		  	  if(madd==0){
		  		  madd=madd+1;
		  	  }


	  	  //Write values from the sensors to the EEPROM

	  	  //WRITING the analog Sensor Data
	  	  memset(str, 0, sizeof(str));
	  	  sprintf(str, "Writing 0x%X to EEPROM address 0x%X", analogData, madd);
	  	  debugPrintln(&huart1, str);
	  	  I2CReturn = HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEVICE_ADDR, madd, 2, sanalogData, 1, HAL_MAX_DELAY);
	  	  if (I2CReturn != HAL_OK) {
	  	  debugPrintln(&huart1, "Write to address FAILED");
	  	  }

	  	  madd=madd+1;

	  //WRITING the Digital Sensor Data

	  memset(str, 0, sizeof(str));
	  sprintf(str, "Writing 0x%X to EEPROM address 0x%X", digitalData, madd);
	  debugPrintln(&huart1, str);
	  I2CReturn = HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEVICE_ADDR, madd, 2, sdigitalData, 1, HAL_MAX_DELAY);
	  if (I2CReturn != HAL_OK) {
	  debugPrintln(&huart1, "Write to address FAILED");
	  }

	  madd=madd+1;

  	  //WRITING the Max Address to position 0x00 in EEPROM
  	  memset(str, 0, sizeof(str));
  	  sprintf(str, "Updating 0x%X to EEPROM address 0x%X", madd, 0x00);
  	  debugPrintln(&huart1, str);
  	  I2CReturn = HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEVICE_ADDR, madd, 2, sanalogData, 1, HAL_MAX_DELAY);
  	  if (I2CReturn != HAL_OK) {
  	  debugPrintln(&huart1, "Write to address FAILED");
  	  }



	  	debugPrintln(&huart1, "-------------"); //Draw a line under a series of 5 before next while loop begins

		  if(HAL_GPIO_ReadPin (GPIOB, GPIO_PIN_4)){

			  debugPrintln(&huart1, "Plug Detect "); // print full line

			  debugPrintln(&huart1, "-------------"); //Draw a line under a series of 5 before next while loop begins

		      // Convert to string and print
		      debugPrintln(&huart1, "Light: "); // print full line
		      sprintf(msg, "%hu\r\n", analogData);
		      HAL_UART_Transmit(&huart1, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);

		      if(testLight>1000){

		    	  debugPrintln(&huart1, "Sunny ;) Good day for the beach. "); // print full line

		      }else if(testLight>5){

		    	  debugPrintln(&huart1, "Overcast. Netflix and chill. "); // print full line

		      }else{

		    	  debugPrintln(&huart1, "Dark. Time to party!!!! "); // print full line
		      }
		  //
		  		HAL_Delay(1000);


		  		sprintf(buffer,
		  			  	 "%d.%dºC, %d.%dºF, %d.%d K, %d.%d%% RH\n\r",
		  			  	 SHT2x_GetInteger(cel), SHT2x_GetDecimal(cel, 1),
		  			  	 SHT2x_GetInteger(fah), SHT2x_GetDecimal(fah, 1),
		  			  	 SHT2x_GetInteger(kel), SHT2x_GetDecimal(kel, 1),
		  			  	 SHT2x_GetInteger(rh), SHT2x_GetDecimal(rh, 1));
		  			  	 HAL_UART_Transmit(&huart1, buffer, strlen(buffer), 1000);
		  			  	 HAL_Delay(10000);

		  	debugPrintln(&huart1, "-------------"); //Draw a line under a series of 5 before next while loop begins

		  	  //Read back last 2 values again:
		  	  madd = madd - 2;

		  	  I2CReturn = HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEVICE_ADDR, madd, 2, ranalogData, 1, HAL_MAX_DELAY);
		  	  if (I2CReturn != HAL_OK) {
		  	  debugPrintln(&huart1, "Read from address FAILED");
		  	  }
		  	  //PRINT READ VALUE
		  	  memset(str, 0, sizeof(str));
		  	  sprintf(str, "Address 0x%X contains: 0x%X ", madd, Result);
		  	  debugPrintln(&huart1, str);
		  	  madd = madd + 1;

		  	I2CReturn = HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEVICE_ADDR, madd, 2, rdigitalData, 1, HAL_MAX_DELAY);
		  	if (I2CReturn != HAL_OK) {
		  	debugPrintln(&huart1, "Read from address FAILED");
		  	}
		  	//PRINT READ VALUE
		  	memset(str, 0, sizeof(str));
		  	sprintf(str, "Address 0x%X contains: 0x%X ", madd, resultDigital);
		  	debugPrintln(&huart1, str);
		  	madd = madd + 1;

		  	debugPrintln(&huart1, "-------------"); //Draw a line under a series of 5 before next while loop begins

		  }


//
//	  //READING
//	  memset(str, 0, sizeof(str));
//	  sprintf(str, "Reading from EEPROM address 0x%X ", madd);
//	  debugPrintln(&huart1, str);
//	  I2CReturn = HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEVICE_ADDR, madd, 2, rData, 1, HAL_MAX_DELAY);
//	  if (I2CReturn != HAL_OK) {
//	  debugPrintln(&huart1, "Read from address FAILED");
//	  }
//	  //PRINT READ VALUE
//	  memset(str, 0, sizeof(str));
//	  sprintf(str, "Received data: 0x%X \n", Result);
//	  debugPrintln(&huart1, str);
//	  //Increment address and data values and clear Result holder
//	  madd = madd + 1;
//	  Data = Data + 1;
//	  Result = 0x00;
//	  HAL_Delay(1000); //Pause
//	  } //End for loop of reading and writing 5 addresses
//
//	  //Read back last 5 values again:
//	  madd = madd - 5;
//	  for (j = 0; j < 5; j++) {
//	  I2CReturn = HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEVICE_ADDR, madd, 2, rData, 1, HAL_MAX_DELAY);
//	  if (I2CReturn != HAL_OK) {
//	  debugPrintln(&huart1, "Read from address FAILED");
//	  }
//	  //PRINT READ VALUE
//	  memset(str, 0, sizeof(str));
//	  sprintf(str, "Address 0x%X contains: 0x%X ", madd, Result);
//	  debugPrintln(&huart1, str);
//	  madd = madd + 1;
//	  }
//	  debugPrintln(&huart1, "-------------"); //Draw a line under a series of 5 before next while loop begins
	  //Flash Blue LED as visual check code is running
	  HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_8);
	  HAL_Delay(1000);



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_14;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5|LD4_Pin|LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA2 PA3 PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA5 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PC5 LD4_Pin LD3_Pin */
  GPIO_InitStruct.Pin = GPIO_PIN_5|LD4_Pin|LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PB4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
