# Tutorial for Weather Station

## Introduction
This guide should be used to familiarize yourself with the weather station and the additional tools required to operate it. The guide will focus on two major sections, namely:

1.	What hardware is required and how to connect this.
2.	What software/tools are required and how to configure this to allow for the correct  operation of            the weather station.

Furthermore, a step-by-step guide of the board’s ‘Hello World’ program is provided to showcase the boards features. Lastly, the boards licensing is detailed. 

## What Hardware you will need

- 	STM32F0 discovery board
-	External boost converter that can boost and regulate at least 3V6-4V2V to 5V0
-	2 1x33 pin Female Header
-	1 1x6 pin Male Header
-	1 18650 Li-ion battery holder and cell (Optional)
-	1 LM341
-	Female micro-USB

## Additional tools required

- 	Point nose pliers
-	Soldering Iron and Solder

## How to connect the Hardware
1.	Start by removing R38 and R41 from the board with the point nose pliers. Hold the board firmly down and, with the pliers, pull the resistor straight up. You should see a white line where the resistor was if it disconnected correctly.

2.	Take the LM341 and cut the top half of the heat sink off with the pliers. As reference, cut in the middle of the circle on the heat sink. Insert the LM341 pins in U2 on the board and bend the pins so that body of the component folds towards the battery, with the heat sink facing up.

3.	Solder the following components:
	
- LM341 				        - 			U2
- Female micro-USB port		    -			J5
- Female pin headers		    -			J1, J3
- Male pin headers		        -			J7
- 18650 Battery Holder*		    -			B1

*The battery holder should be orientated so that the negative side (Ground) is on the circular pad.

4.	Solder pins 2 and 3 of the switch (S1) together. These are the two pins located closest to the right edge of the board.

5.	Plug in the STM32f0 into the two rows of female pin headers. The orientation of the discovery board should be the same as that of the weather station.

6.	Lastly, with jumpers connect pin 3 of J7 to input of boost converter, and the output of the boost converter to pin 2 on J7. Connect the ground of the converter to the ground on the STM32f0 board.

You have now correctly set up the hardware of the weather station!

## What software you will need

- 	STM32CubeIDE 
-	PuTTY

## Configuring the software 
STM32CubeIDE – Download and install the IDE. After opening select: STM Project -> MCU/MPU Selector -> Search for STM32F051R8T6 -> Select the STM32F0 Discovery Board-> Next -> Name the project (Weather Station) -> Finish -> Click Yes on ‘initialise peripheral defaults’. Configure the ports as shown in the repository. Download the code available in the repository and open it in STM32CubeIDE. Build and run the code while the computer is connected to the microcontroller.

PuTTY - After downloading PuTTY, open the application and set the following configurations on the tab that opens on your screen:
- Connection Type: Serial
- Serial Line: COM8
- Speed: 9600
- Close Windows on exit: Always
Press ‘OPEN’ to receive data from the weather station once powered up.

## ‘Hello World’ Program
After connecting the hardware and configuring the software, the board is ready to display its full functionality. Connect the HAT via micro-USB. A red LED will light up to indicate that the HAT is connected Open PuTTY and configure to the settings above. Every 10 seconds, data will be updated. Hold a finger over Q1 for 10 seconds to demonstrate the change in light. Blow on IC3 for 10 seconds to demonstrate a change in humidity and temperature. By applying 4.1V to the battery power supply (either by using a battery or bench power supply), a white LED will light up to indicate that the HAT is fully charged.

## License
Copyleft software license with the following stipulations:
1.	Redistribution of software will include a reference to the original authors.

2.	Any changes to the software which  improve the efficiency of the original code or improve the functionality of the board must be added through a git pull request.

3.	This team may use any modifications made to the code, provided they provide adequate reference to the modifications’ authors.
